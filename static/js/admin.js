var app = angular.module('exboard', ['ngRoute', "checklist-model", 'ngAnimate', 'toaster']);

app.config(function($routeProvider) {
    $routeProvider
        // route for the about page
        .when('/login', {
            controller : 'authCtrl',
            templateUrl : 'adm-login.html'
        })

        .when('/', {
        })

        .when('/categories', {
            templateUrl : 'adm-categories.html'
        })

        .when('/add-categories', {
            templateUrl : 'adm-add-categories.html',
            controller  : 'AddCategoryController'
        })

        .when('/insert-categories', {
            templateUrl : 'adm-insert-categories.html',
            controller  : 'InsertCategoryController'
        })

        .when('/remove-categories', {
            templateUrl : 'adm-remove-categories.html',
            controller  : 'CategoriesController'
        })

        .otherwise({
            redirectTo: '/login'
        });
});
app.controller('BodyController', function($scope, $http, $location, $rootScope) {
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        $scope.authenticated = false;
        $http({
            method: 'POST',
            url: '/apiadmin/me',
            data: {},
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data, status, headers, config){
            if(data.user_name) {
                $scope.authenticated = true;
                $scope.user_id = data.user_id;
                $scope.user_name = data.user_name;
                if($location.url() == '/login') {
                    $location.path("/");
                }
            } else {
                $location.path("/login");
            }
        });
    });
});
//app.run(function ($rootScope, $location, $http, Data) {
//    $rootScope.$on("$routeChangeStart", function (event, next, current) {
//        $rootScope.authenticated = false;
//        $http({
//            method: 'POST',
//            url: '/apiadmin/me',
//            data: {},
//            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
//        }).success(function(data, status, headers, config){
//            if(data.user_name) {
//                $rootScope.authenticated = true;
//                $rootScope.user_id = data.user_id;
//                $rootScope.user_name = data.user_name;
//                if($location.url() == '/login') {
//                    $location.path("/");
//                }
//            } else {
//                $location.path("/login");
//            }
//        });
//    });
//});

app.factory("Data", ['$http', 'toaster',
    function ($http, toaster) { // This service connects to our REST API

        var serviceBase = '/';

        var obj = {};
        obj.toast = function (data) {
            toaster.pop(data.status, "", data.message, 10000, 'trustedHtml');
        }
        obj.get = function (q) {
            return $http.get(serviceBase + q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(serviceBase + q, object).then(function (results) {
                return results.data;
            });
        };
        obj.put = function (q, object) {
            return $http.put(serviceBase + q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(serviceBase + q).then(function (results) {
                return results.data;
            });
        };

        return obj;
    }]);

app.controller('authCtrl', function ($scope, $rootScope, $routeParams, $location, $http, Data) {
    //initially set those objects to null to avoid undefined error
    $scope.login = {};

    $scope.doLogin = function (login) {
        $http({
            method: 'POST',
            url: '/apiweb/me/login',
            data: login,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data, status, headers, config) {
            $location.path("/");
        });
    };
});

app.controller('indexController', ['$scope', function($scope) {

    }])
    .directive('index', function() {

    });

app.controller('categoriesController', ['$scope', function($scope) {

    }])
    .directive('categories', function() {

    });

app.controller('AddCategoryController', ["$scope", "$http", function($scope, $http) {
    $scope.item = {
        parent_id: 0,
        is_hidden: 0,
        filters: [],
        action: 'add'
    };

    $scope.add = function(item) {
        if(item.title) {
            item.url = urlLit(item.title, 0);
        }
        if(item.filters.length > 0) {
            var filters = item.filters[0];
            if(item.filters[1]) {
                for (var i = 1; i < item.filters.length; i++) {
                    filters = filters + ',' + item.filters[i];
                }
            }
            item.filters = filters;
        }

        $http({
            method: 'POST',
            url: '/apiadmin/categories/' + item.action,
            data: item,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data){
            $scope.master = $scope.data;
        })
    }
}]);

app.controller('InsertCategoryController', ["$scope", "$http", function($scope, $http) {
    $scope.item = {
        parent_id: 0,
        is_hidden: 0,
        filters: [],
        action: 'insert'
    };

    $scope.add = function(item) {
        if(item.title) {
            item.url = urlLit(item.title, 0);
        }
        if(item.filters.length > 0) {
            var filters = item.filters[0];
            if(item.filters[1]) {
                for (var i = 1; i < item.filters.length; i++) {
                    filters = filters + ',' + item.filters[i];
                }
            }
            item.filters = filters;
        }
        $http({
            method: 'POST',
            url: '/apiadmin/categories/' + item.action,
            data: item,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data){
            $scope.master = $scope.data;
        })
    }
}])

app.controller('CategoriesController', ["$scope", "$http", function($scope, $http) {
    $scope.categories = [];

    $http({
        method: 'POST',
        url: '/apiweb/categories',
        data: {type:'tree'}
    }).success(function(data){
        $scope.categories = [data];

    });
    // $scope.categories = [{"result":"success","data":[{"title":"child","id":3},{"title":"men","id":2,"children":[{"title":"men_clothes","id":11,"children":""},{"title":"men_shoes","id":12,"children":[{"title":"men_boots","id":13,"children":""}]}]},{"title":"women","id":1,"children":[{"title":"women_clothes","id":8,"children":[{"title":"women_jacket","id":5,"children":""},{"title":"women_trousers","id":7,"children":""}]},{"title":"women_shoes","id":9,"children":[{"title":"women_boots","id":6,"children":""}]},{"title":"women_toys","id":10,"children":""}]}]}];
    $scope.show = function(id) {
        document.getElementById(id).style.display = 'block';
    }
    $scope.hide = function(id) {
        document.getElementById(id).style.display = 'none';
    }
    $scope.remove = function(id, title) {
        if(confirm("Delete category " + title + "?")) {
            $http({
                method: 'POST',
                url: '/apiadmin/categories/delete',
                data: {id: id}
            }).success(function (data) {
                $http({
                    method: 'POST',
                    url: '/apiweb/categories',
                    data: {type:'tree'}
                }).success(function(data){
                    $scope.categories = [data];

                });

            });
        }
    }
}]);

app.controller('CategoriesListController', ["$scope", "$http", function($scope, $http) {
    $scope.categories = [];
    //$scope.categories = [{"result":"success","data":[{"title":"child","url":null,"description":"childs clothes","keywords":null,"parent_id":0,"children":"","level":1,"filters":null,"sort":null,"hiden":false,"id":3},{"title":"women_toys","url":null,"description":"women toys","keywords":null,"parent_id":1,"children":"","level":2,"filters":null,"sort":null,"hiden":false,"id":10},{"title":"men_clothes","url":null,"description":"men clothes","keywords":null,"parent_id":2,"children":"","level":2,"filters":null,"sort":null,"hiden":false,"id":11},{"title":"women_clothes","url":null,"description":"women clothes","keywords":null,"parent_id":1,"children":"5,7","level":2,"filters":null,"sort":null,"hiden":false,"id":8},{"title":"women_shoes","url":null,"description":"women shoes","keywords":null,"parent_id":1,"children":"6","level":2,"filters":null,"sort":null,"hiden":false,"id":9},{"title":"men","url":null,"description":"mens clothes","keywords":null,"parent_id":0,"children":"11,12","level":1,"filters":null,"sort":null,"hiden":false,"id":2},{"title":"men_boots","url":null,"description":"men boots","keywords":null,"parent_id":12,"children":"","level":3,"filters":null,"sort":null,"hiden":false,"id":13},{"title":"men_shoes","url":null,"description":"men shoes","keywords":null,"parent_id":2,"children":"13","level":2,"filters":null,"sort":null,"hiden":false,"id":12},{"title":"women_trousers","url":null,"description":"trousers","keywords":null,"parent_id":8,"children":"","level":3,"filters":null,"sort":null,"hiden":false,"id":7},{"title":"women_jacket","url":null,"description":"jackets","keywords":null,"parent_id":8,"children":"","level":3,"filters":null,"sort":null,"hiden":false,"id":5},{"title":"women_boots","url":null,"description":"shoes","keywords":null,"parent_id":9,"children":"","level":3,"filters":null,"sort":null,"hiden":false,"id":6},{"title":"women","url":null,"description":"womens clothes","keywords":null,"parent_id":0,"children":"8,9,10","level":1,"filters":null,"sort":null,"hiden":false,"id":1}]}];
    $http({
        method: 'POST',
        url: '/apiweb/categories',
        data: {type:'list'}
    }).success(function(data){
        $scope.categories = [data];

    });


}]);

app.controller('FiltersController', function($scope, $http) {
    $scope.filters = [];
    $http({
        method: 'POST',
        url: '/apiweb/filters',
        data: {}
    }).success(function(data){
        $scope.filters = [data];
    });

});

function urlLit(w,v) {
    var tr='a b v g d e ["zh","j"] z i y k l m n o p r s t u f h c ch sh ["shh","shch"] ~ y ~ e yu ya ~ ["jo","e"]'.split(' ');
    var ww=''; w=w.toLowerCase().replace(/ /g,'_');
    for(i=0; i<w.length; ++i) {
        cc=w.charCodeAt(i); ch=(cc>=1072?tr[cc-1072]:w[i]);
        if(ch.length<3) ww+=ch; else ww+=eval(ch)[v];
    }
    return(ww.replace(/~/g,''));
}