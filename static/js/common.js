var app = angular.module('exboard', ["checklist-model"]);

app.controller('CategoriesController', ["$scope", "$http", function($scope, $http) {
    $scope.categories = [];

    $http({
        method: 'POST',
        url: '/apiweb/categories',
        data: {type:'tree'}
    }).success(function(data){
        $scope.categories = [data];

    });
   // $scope.categories = [{"result":"success","data":[{"title":"child","id":3},{"title":"men","id":2,"children":[{"title":"men_clothes","id":11,"children":""},{"title":"men_shoes","id":12,"children":[{"title":"men_boots","id":13,"children":""}]}]},{"title":"women","id":1,"children":[{"title":"women_clothes","id":8,"children":[{"title":"women_jacket","id":5,"children":""},{"title":"women_trousers","id":7,"children":""}]},{"title":"women_shoes","id":9,"children":[{"title":"women_boots","id":6,"children":""}]},{"title":"women_toys","id":10,"children":""}]}]}];
    $scope.show = function(id) {
        document.getElementById(id).style.display = 'block';
    }
    $scope.hide = function(id) {
        document.getElementById(id).style.display = 'none';
    }
    $scope.remove = function(id, title) {
        if(confirm("Delete category " + title + "?")) {
            $http({
                method: 'POST',
                url: '/apiweb/categories/delete',
                data: {id: id}
            }).success(function (data) {
                $http({
                    method: 'POST',
                    url: '/apiweb/categories',
                    data: {type:'tree'}
                }).success(function(data){
                    $scope.categories = [data];

                });

            });
        }
    }
}]);

app.controller('CategoriesListController', ["$scope", "$http", function($scope, $http) {
    $scope.categories = [];
    //$scope.categories = [{"result":"success","data":[{"title":"child","url":null,"description":"childs clothes","keywords":null,"parent_id":0,"children":"","level":1,"filters":null,"sort":null,"hiden":false,"id":3},{"title":"women_toys","url":null,"description":"women toys","keywords":null,"parent_id":1,"children":"","level":2,"filters":null,"sort":null,"hiden":false,"id":10},{"title":"men_clothes","url":null,"description":"men clothes","keywords":null,"parent_id":2,"children":"","level":2,"filters":null,"sort":null,"hiden":false,"id":11},{"title":"women_clothes","url":null,"description":"women clothes","keywords":null,"parent_id":1,"children":"5,7","level":2,"filters":null,"sort":null,"hiden":false,"id":8},{"title":"women_shoes","url":null,"description":"women shoes","keywords":null,"parent_id":1,"children":"6","level":2,"filters":null,"sort":null,"hiden":false,"id":9},{"title":"men","url":null,"description":"mens clothes","keywords":null,"parent_id":0,"children":"11,12","level":1,"filters":null,"sort":null,"hiden":false,"id":2},{"title":"men_boots","url":null,"description":"men boots","keywords":null,"parent_id":12,"children":"","level":3,"filters":null,"sort":null,"hiden":false,"id":13},{"title":"men_shoes","url":null,"description":"men shoes","keywords":null,"parent_id":2,"children":"13","level":2,"filters":null,"sort":null,"hiden":false,"id":12},{"title":"women_trousers","url":null,"description":"trousers","keywords":null,"parent_id":8,"children":"","level":3,"filters":null,"sort":null,"hiden":false,"id":7},{"title":"women_jacket","url":null,"description":"jackets","keywords":null,"parent_id":8,"children":"","level":3,"filters":null,"sort":null,"hiden":false,"id":5},{"title":"women_boots","url":null,"description":"shoes","keywords":null,"parent_id":9,"children":"","level":3,"filters":null,"sort":null,"hiden":false,"id":6},{"title":"women","url":null,"description":"womens clothes","keywords":null,"parent_id":0,"children":"8,9,10","level":1,"filters":null,"sort":null,"hiden":false,"id":1}]}];
    $http({
        method: 'POST',
        url: '/apiweb/categories',
        data: {type:'list'}
    }).success(function(data){
        $scope.categories = [data];

    });


}]);

app.controller('AddItemController', ["$scope", "$http", function($scope, $http) {
    $scope.item = {
        cat_id: 0,
        filters: {}
    };
    $scope.item2 = {
        cat_id: 0,
        filters: {}
    };

    $scope.filtersradio = [];
    $scope.filterscheckbox = [];
    $scope.filterslistbox = [];
    $scope.filterstextbox = [];
    $scope.filtersradio_2 = [];
    $scope.filterscheckbox_2 = [];
    $scope.filterslistbox_2 = [];
    $scope.filterstextbox_2 = [];

    $scope.getFilters = function (cat_id, step) {

        $http({
            method: 'POST',
            url: '/apiweb/filters/show',
            data: {cat_id:cat_id}
        }).success(function(data){
            if(step == 1) {
                for (var i = 0; i < data.data.length; i++) {
                    switch(data.data[i].representation) {
                        case 'listbox':
                            $scope.filterslistbox.push(data.data[i]);
                            break;
                        case 'checkbox':
                            $scope.filterscheckbox.push(data.data[i]);
                            break;
                        case 'radio':
                            $scope.filtersradio.push(data.data[i]);
                            break;
                        case 'textbox':
                            $scope.filterstextbox.push(data.data[i]);
                            break;
                    }
                }
            } else {
                for (var i = 0; i < data.data.length; i++) {
                    switch(data.data[i].representation) {
                        case 'listbox':
                            $scope.filterslistbox_2.push(data.data[i]);
                            break;
                        case 'checkbox':
                            $scope.filterscheckbox_2.push(data.data[i]);
                            break;
                        case 'radio':
                            $scope.filtersradio_2.push(data.data[i]);
                            break;
                        case 'textbox':
                            $scope.filterstextbox_2.push(data.data[i]);
                            break;
                    }
                }
            }
        });
    }
    $scope.add = function(item, item2) {

        if(item.title) {
            item.url = urlLit(item.title, 0);
        }
        if(item2.title) {
            item2.url = urlLit(item2.title, 0);
        }
        $http({
            method: 'POST',
            url: '/apiweb/items/add',
            data: {give: item, receive: item2},
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data){
            $scope.master = $scope.data;
        })
    }
    $scope.secondStep = function() {
        if($scope.item.title && $scope.item.description) {
            document.getElementById("secondStep").style.display = 'block';
            document.getElementById("firstStep").style.display = 'none';
        }
    }
}]);

app.controller('AddCategoryController', ["$scope", "$http", function($scope, $http) {
    $scope.item = {
        parent_id: 0,
        is_hidden: 0,
        filters: [],
        action: 'add'
    };

    $scope.add = function(item) {
        if(item.title) {
            item.url = urlLit(item.title, 0);
        }
        if(item.filters.length > 0) {
            var filters = item.filters[0];
            if(item.filters[1]) {
                for (var i = 1; i < item.filters.length; i++) {
                    filters = filters + ',' + item.filters[i];
                }
            }
            item.filters = filters;
        }

        $http({
            method: 'POST',
            url: '/apiweb/categories/' + item.action,
            data: item,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data){
            $scope.master = $scope.data;
        })
    }
}]);

app.controller('FiltersController', function($scope, $http) {
    $scope.filters = [];
    $http({
        method: 'POST',
        url: '/apiweb/filters',
        data: {}
    }).success(function(data){
        $scope.filters = [data];
    });

});


app.controller('InsertCategoryController', ["$scope", "$http", function($scope, $http) {
    $scope.item = {
        parent_id: 0,
        is_hidden: 0,
        filters: 0,
        action: 'insert'
    };

    $scope.add = function(item) {
        if(item.title) {
            item.url = urlLit(item.title, 0);
        }
        $http({
            method: 'POST',
            url: '/apiweb/categories/' + item.action,
            data: item,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data){
            $scope.master = $scope.data;
        })
    }
}]);

app.controller('RegistrationController', ["$scope", "$http", function($scope, $http) {
    $scope.master = {};
    $scope.update = function(user) {
        $scope.master = angular.copy(user);
        $http({
            method: 'POST',
            url: '/apiweb/me/registration',
            data: user,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data, status, headers, config){
            $scope.master = data;
        }).error(function(data, status, headers, config){
            $scope.master = data;
        });
    };

    $scope.reset = function() {
        $scope.user = angular.copy($scope.master);
    };
    $scope.reset();
}]);

app.controller('LoginController', ["$scope", "$http", function($scope, $http) {

    $scope.cookie = {};
    $scope.update = function(login) {
        $scope.cookie = angular.copy(login);
        $http({
            method: 'POST',
            url: '/apiweb/me/login',
            data: login,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data, status, headers, config){
            document.getElementById('loginForm').style.display = 'none';
            var user = angular.element('<p><a href="#">' + data.user_name + '</a><span ng-controller="LogoutController"><a onclick="logout()" href="#">Выйти</a></span></p>');
            angular.element(document.getElementById('cabinet')).append(user, data);
        }).error(function(data, status, headers, config){

        });
    };
    $scope.reset = function() {
        $scope.login = angular.copy($scope.cookie);
    };
    $scope.reset();
}]);

app.controller('LogoutController', ["$scope", "$http", function($scope, $http) {
    $scope.logout = function() {
        $http({
            method: 'POST',
            url: '/apiweb/me/logout',
            data: {},
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data, status, headers, config){
            alert('success');
        }).error(function(data, status, headers, config){

        });
    };
}]);

function logout() {
    app.controller('LogoutController', ["$scope", "$http", function($scope, $http) {
        $http({
            method: 'POST',
            url: '/apiweb/me/logout',
            data: {},
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data, status, headers, config) {
            alert('success');
        }).error(function (data, status, headers, config) {

        });
    }]);
}

app.controller('GetCookieController', ["$scope", "$http", function($scope, $http) {
    $http({
        method: 'POST',
        url: '/apiweb/me',
        data: {},
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(data, status, headers, config){
        if(data.user_name) {
            document.getElementById('loginForm').style.display = 'none';
            var user = angular.element('<p><a href="#">' + data.user_name + '</a><span ng-controller="LogoutController"><a ng-click="logout()" href="#">Выйти</a></span></p>');
            angular.element(document.getElementById('cabinet')).append(user, data);
        } else {
            document.getElementById('loginForm').style.display = 'block';
        }
    }).error(function(data, status, headers, config){

    });

}]);

app.controller('UploadController', ["$scope", "$http", function($scope, $http) {
    $scope.master = {};
    $scope.upload = function(file) {
        $scope.file = file;
        var uploadUrl = '/upload/attach';
        //fileUpload.uploadFileToUrl(file, uploadUrl);
        $http({
            method: 'POST',
            url: '/upload/attach',
            file: file,
            headers: {'Content-Type': 'multipart/form-data'}
        }).success(function(data, status, headers, config){
            $scope.master = data;
        }).error(function(data, status, headers, config){
            $scope.master = data;
        });
    };

    $scope.reset = function() {
        $scope.user = angular.copy($scope.master);
    };
    $scope.reset();
}]);


app.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': 'multipart/form-data'}
        })
            .success(function(){
            })
            .error(function(){
            });
    }
}]);

app.controller('uploadCtrl', [
        '$scope',
        '$upload',
        function ($scope, $upload) {
            $scope.selectedFile = [];
            $scope.uploadProgress = 0;
            $scope.uploadFile = function () {
                var file = $scope.selectedFile[0];
                $scope.upload = $upload.upload({
                    url: '/upload/attach',
                    method: 'POST',
                    file: file,
                    headers: {'Content-Type': 'multipart/form-data'}
                }).progress(function (evt) {
                    $scope.uploadProgress = parseInt(100.0 * evt.loaded / evt.total, 10);
                }).success(function (data) {
                    alert(1);
                });
            };
            $scope.onFileSelect = function ($files) {

                $scope.selectedFile = $files;
            };

        }
    ]);

function urlLit(w,v) {
    var tr='a b v g d e ["zh","j"] z i y k l m n o p r s t u f h c ch sh ["shh","shch"] ~ y ~ e yu ya ~ ["jo","e"]'.split(' ');
    var ww=''; w=w.toLowerCase().replace(/ /g,'_');
    for(i=0; i<w.length; ++i) {
        cc=w.charCodeAt(i); ch=(cc>=1072?tr[cc-1072]:w[i]);
        if(ch.length<3) ww+=ch; else ww+=eval(ch)[v];
    }
    return(ww.replace(/~/g,''));
}
