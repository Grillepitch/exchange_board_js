app.config(function($routeProvider, $locationProvider) {
    $routeProvider.when('/add-items.html', {
            templateUrl: 'book.html',
            controller: 'BookController',
            resolve: {
                // I will cause a 1 second delay
                delay: function ($q, $timeout) {
                    var delay = $q.defer();
                    $timeout(delay.resolve, 1000);
                    return delay.promise;
                }
            }
        })
    $routeProvider.when('/add-items.html#step-2', {
            templateUrl: 'chapter.html',
            controller: 'ChapterController'
        });

    // configure html5 to get links working on jsfiddle
    $locationProvider.html5Mode(true);
});